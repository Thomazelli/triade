<?php  include("_includes/header.php") ?>
<?php  include("_includes/banner.php") ?>

<section id="main-features">
    <div class="divider_top_black"></div>

    <div class="container">
        <div class="row">
            <div class=" col-md-10 col-md-offset-1 text-center">
                <h2>Triade Solutions</h2>
                <p class="lead" style="font-size:15px; text-align:justify;">
                    A <strong>Triade Solutions</strong> é uma empresa de desenvolvimento de softwares com ênfase em 
                    processos gerenciais imobiliários. Nosso negócio é desenvolver soluções que visam o crescimento 
                    dos nossos clientes e prover soluções tecnológicas que atendam suas expectativas.

                    Ao longo de 10 anos no mercado tecnológico, destaca-se pelo perfil ousado e inovador, a
                    cada dia surpreendendo e atraindo novos clientes e parceiros.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="feature">
                    <i class="icon-trophy"></i>
                    <h3>Profissionais especializados</h3>
                    <p>
                        Todos nossos profissionais são especializados no que fazem para oferecer a nossos clientes um produto de alta qualidade e eficiência garantida.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="feature">
                    <i class=" icon-ok-4"></i>
                    <h3>Segurança e Agilidade</h3>
                    <p>
                        Trabalhamos focados em manter seu negócio seguro sem perder a agilidade no atendimento a nossos clientes... de um novo desenvolvimento até um simples suporte.
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container-->

</section><!-- End main-features -->

<section style="padding:20px 0 50px 0;">
    <div class="container">

        <div class="row">
            <div class="col-md-12 text-center">
                <h1 style="text-transform:none;">iMovel</h1>
                <p class="lead">Software de Gestão Imobiliária</p>
            </div>
        </div><!-- End row -->

        <div class="row" id="sub-header-features-2">
            <div class="col-md-6">
                <h2>O que é o iMovel?</h2>
                <p>O iMóvel, sistema de gestão imobiliário, atende desde corretores autônomos até imobiliárias de grande porte. Por ser um produto personalizável, é capaz de atender a cada cliente individualmente, de acordo com suas expectativas. Possuindo uma interface amigável, o usuário realiza com rapidez e segurança: cadastros, pesquisas, inserção de fotos, relatórios, gráficos, entre outros. O iMóvel contribui com a redução de tempo e custos, sendo um forte aliado na tomada de decisão.</p>
                <p><em>Cabral Xavier - General Manager</em></p>
            </div>
            <div class="col-md-6">
                <h2>Nossos diferenciais</h2>
                <p>O mercado está cheio de sistemas que prometem gerir seu negócio. O que o iMóvel oferece a mais?</p>
                <ul class="list_ok">
                    <li><strong>Tecnologia</strong> Usamos plataformas web que podem ser acessadas de diversos tipos de aparelhos computacionais, o que facilita a integração ao sistema, à empresa e sua adaptabilidade com relação aos usuários do sistema.</li>
                    <li><strong>Marketing Imobiliário</strong> Nossa tendência é a criação de produtos que atendam o setor de gestão e marketing imobiliário, na qual se tornará componente da nossa aplicação. Isso trará benefícios aos nossos clientes interligando seus processos de marketing as nossas ferramentas.</li>
                </ul>
            </div>
            <div class="col-md-12" style="text-align:Center; margin-top:30px;">
                <a href="comprar.php" class="button_outline">Compre agora!</a>
            </div>

        </div><!-- End row -->
    </div><!-- End container -->		
</section>

<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class='col-md-offset-2 col-md-8 text-center'>
                <h2>O que dizem</h2>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-offset-2 col-md-8'>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                    </ol>
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="img/teacher_3_small.jpg" alt="">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>
                                            Conseguimos uma enorme economia de tempo ao atender nossos clientes com o iMóvel.
                                        </p>
                                        <small>Mariana Cintra (MRR Construtora e Empreendimentos Imobiliários)</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <blockquote>
                                <div class="row">
                                    <div class="col-sm-3 text-center">
                                        <img class="img-circle" src="img/teacher_4_small.jpg" alt="">
                                    </div>
                                    <div class="col-sm-9">
                                        <p>
                                            Suporte atencioso e ágil. Uma ótima decisão a contratação do iMóvel!!
                                        </p>
                                        <small>Cinthia Coimbra (Casa Verde Negócios Imobiliários)</small>
                                    </div>
                                </div>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End testimonials -->

<?php  include("_includes/footer.php") ?>