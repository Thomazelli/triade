<section class="tp-banner-container">
    <div class="tp-banner" >
        <ul>	<!-- SLIDE  -->
            <li data-transition="fade" data-slotamount="4" data-masterspeed="1500" >
                <!-- MAIN IMAGE -->
                <img src="sliderimages/slide_5.jpg" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="100"
                     data-y="190"
                     data-hoffset="30"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">Soluções de Software
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="100"
                     data-y="245"
                     data-hoffset="30"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">Foco em tecnologia
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="100"
                     data-y="300"
                     data-hoffset="30"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1500"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">10 anos de mercado
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption medium_bg_darkblue skewfromright customout"
                     data-x="right"
                     data-y="190"
                     data-hoffset="-100"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">iMovel
                </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption medium_bg_darkblue skewfromright customout"
                     data-x="right"
                     data-y="245"
                     data-hoffset="-100"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">Gestão imobiliária
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption medium_bg_darkblue skewfromleft customout"
                     data-x="right"
                     data-y="300"
                     data-hoffset="-100"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="800"
                     data-start="1800"
                     data-easing="Power4.easeOut"
                     data-endspeed="300"
                     data-endeasing="Power1.easeIn"
                     data-captionhidden="on"
                     style="z-index: 6">Propostas inovadoras
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-transition="zoomout" data-slotamount="4" data-masterspeed="1000" >
                <!-- MAIN IMAGE -->
                <img src="sliderimages/slide_6.jpg" alt="slidebg2"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption medium_light_white skewfromrightshort customout"
                     data-x="80"
                     data-y="96"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="800"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 4">Enjoy
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption medium_thin_white skewfromleftshort customout"
                     data-x="235"
                     data-y="110"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="900"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 5">&
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption large_bold_white skewfromleftshort customout"
                     data-x="80"
                     data-y="152"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="1100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 7">conheça-nos
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption small_thin_white  customin customout"
                     data-x="80"
                     data-y="240"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">A Logical Solutions é uma empresa a 10 anos no mercado<br />oferecendo soluções tecnológicas que irão diminuir o<br />custo e o tempo utilizado<br />do seu dia-a-dia.<br />Faça-nos uma visita!
                </div>
            </li>

            <!-- SLIDE  -->
            <li data-transition="cube-horizontal" data-slotamount="4" data-masterspeed="1000" >
                <!-- MAIN IMAGE -->
                <img src="sliderimages/slide_1.jpg" alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                <!-- LAYERS -->

                <!-- LAYER NR. 3 -->
                <div class="tp-caption large_bold_white skewfromleftshort customout"
                     data-x="80"
                     data-y="152"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="300"
                     data-start="1100"
                     data-easing="Back.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 7; text-transform:none;">iMovel
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption small_thin_white  customin customout"
                     data-x="80"
                     data-y="240"
                     data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="1300"
                     data-easing="Power4.easeOut"
                     data-endspeed="500"
                     data-endeasing="Power4.easeIn"
                     data-captionhidden="on"
                     style="z-index: 8">
                    Conheça o seu próximo software de Gestão Imobiliária<br />
                    e todas as funcionalidades que temos a oferecer.<br /><br />
                    <a href="javascript:newsletter();" class="button_outline">Compre agora!</a>
                </div>
            </li>				
        </ul>
        <div class="tp-bannertimer"></div>
    </div>
</section><!-- End slider -->