<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Inscreva-se em nossa newsletter.</h3>
                <div id="message-newsletter"></div>
                <form method="post" action="" name="newsletter" class="form-inline" onSubmit="return false;">
                    <input name="email_newsletter" id="email_newsletter" type="email" value="" placeholder="Seu e-mail" class="form-control">
                    <a href="javascript:newsletter();" class="button_outline">Inscrever-se</a>
                </form>

                <script>
                    function newsletter() {
                        if (!$("#email_newsletter").val()) {
                            alert("Preencha o e-mail corretamente")
                        } else {
                            alert("Inscrição feita com sucesso!");
                        }

                        return false;
                    }
                </script>
            </div>
        </div>
    </div>

    <hr />

    <div id="copy_right">Todos os direitos reservados © 2015</div>
</footer>

<div id="toTop">Ir para o topo</div>

<!-- JQUERY -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function () {

        revapi = jQuery('.tp-banner').revolution(
            {
                delay: 9000,
                startwidth: 1700,
                startheight: 600,
                hideThumbs: true,
                navigationType: "none",
                fullWidth: "on",
                forceFullWidth: "on"
            });
    }); //ready
</script>

<!-- OTHER JS --> 
<script src="js/superfish.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.placeholder.js"></script>
<script src="js/functions.js"></script>
<script src="js/classie.js"></script>
</body>
</html>