<?php

	include("functions/planos.class.php");
	include("functions/usuarios.class.php");
	include("functions/pedidos.class.php");	

	$idplano = (int) $_GET['plano'];
	$planos = Planos::listar(" WHERE plan_id = " . $idplano );	

	if(!$planos){
		die("Plano inv�lido!");
	}	

	$valida = Usuarios::listar(" WHERE usu_email = '".$_POST['email']."' OR usu_cpf = '".$_POST['cpf']."' ");

	if(count($valida) > 0){
		?>
			<script>
				alert('Usu�rio j� cadastrado');
				location.href = 'finalizar.php?plano=<?=$idplano?>';
			</script>
		<?
		die();
	}	

	$class = new Usuarios();	

	if($_POST['id']){
		$class->setUsu_id($_POST['id']);
	}
	
	$class->setUsu_Cpf($_POST['cpf']);
	$class->setUsu_Nome($_POST['nome']);
	$class->setUsu_Email($_POST['email']);
	$class->setUsu_Nascimento($_POST['nascimento']);
	$class->setUsu_Telefone($_POST['telefone']);
	$class->setUsu_Endereco($_POST['endereco']);
	$class->setUsu_Numero($_POST['numero']);
	$class->setUsu_Complemento($_POST['complemento']);
	$class->setUsu_Bairro($_POST['bairro']);
	$class->setUsu_Estado($_POST['estado']);
	$class->setUsu_Cidade($_POST['cidade']);
	
	try{
		$class->salvar();
	}
	catch( Exception $e ){
		$error =  utf8_encode($e->getMessage());
	}	

	if(!$error){
		$iduser = $GLOBALS['TEMP_VAR_A'];
		
		$pedido = new Pedidos();		

		$pedido->setUsu_id($iduser);
		$pedido->setPlan_id($planos[0]->getPlanId());
		$pedido->setPed_Valor($planos[0]->getValor());
		$pedido->setPed_data_compra(time());
		$pedido->setStatus_id(2);		

		try{
			$pedido->salvar();
		}
		catch( Exception $e ){
			$error =  utf8_encode($e->getMessage());
		}		

		if(!$error){
			$idpedido = $GLOBALS['TEMP_VAR_A'];			

			/* ENVIA EMAIL */
			$headers = "MIME-Version: 1.1\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: contato@triadesolutions.com.br\r\n"; // remetente
			$headers .= "Return-Path: contato@triadesolutions.com.br\r\n"; // return-path			

			$texto = '
				Ol�, ' .$_POST['nome']. ', <br />
				Voc� fez a solicita��o do plano <strong>'.$planos[0]->getTitulo().'</strong> do iMovel!<br /><br />
				Para acompanhar a situa��o do seu pedido, <a href="http://gustavothomazelli.com.br/triade/pedido.php?id=' .$idpedido. '" target="_blank">clique aqui</a>.<br /><br /><br />
				<strong>Equipe Triade Solutions</strong>
			';			

			mail($_POST['email'], "iMovel | Solicita��o de acesso", $texto, $headers);

			header("location:pedido.php?id=" . $idpedido);
		}else{
			die("Falha ao inserir pedido");
		}
	}else{
		die("Falha ao inserir usu�rio");
	}	

	die();