<?php
	include("functions/planos.class.php");
	
	$planos = Planos::listar();
?>

<?php  include("_includes/header.php") ?>

<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Planos e Preços</h1>
				<p class="lead boxed">Selecione um dos planos abaixo</p>
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
	<div class="divider_top"></div>
</section><!-- End sub-header -->

<section id="main_content">
	<div class="container">
		<div class="row text-center plans">
		
			<? foreach($planos as $item){ ?>
			
				<?php
					$carac = Planos::listar_caracteristicas($item->getPlanId());
				?>
			
				<div class="plan <?= ($item->getPlanId() == 2) ? 'plan-tall' : '' ?> col-md-4">
					<h2 class="plan-title"><?=$item->getTitulo()?></h2>
					<p class="plan-price">$<?=round($item->getValor(), 0)?><span>/00</span></p>
					<ul class="plan-features">
						<? foreach($carac as $item_car){ ?>
							<li><?=$item_car->getCaracteristicas()?></li>
						<? } ?>
					</ul>
					<p class=" col-md-8 col-md-offset-2 text-center"><a href="finalizar.php?plano=<?=$item->getPlanId()?>" class="button_medium">Contratar</a></p>
				</div> <!-- End col-md-4 -->
			<? } ?>
			
		</div><!-- End row plans-->		
		
		<hr>
		
		<div class="row">
			<div class="col-md-12">
				<h3>Perguntas frequentes</h3>
			</div>
		</div><!-- end row -->
		
		<div class="row">
		
			<div class="col-md-4">
				<div class="question_box">
					<h3>O que é a marca Triade Solutions</h3>
					<p>Nos planos com a marca Triade Solutions visível, no rodapé do site será exibido que o site é um produto da Triade Solutions Tecnologia e Inteligência.</p>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="question_box">
					<h3>Hospedagem dedicada?</h3>
					<p>O plano com hospedagem dedicada é indicado para grandes clientes com muitas visualizações de seu site. Ela oferece uma hospedagem exclusiva para seu site e suporte da nossa equipe de redes e infraestrutura.</p>
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="question_box">
					<h3>Site customizado?</h3>
					<p>O plano com site customizado te dará o direito a, além de acesso aos layouts disponíveis, solicitar customizações para nossa equipe de desenvolvimento e ter um site totalmente exclusivo para seu negócio.</p>
				</div>
			</div>
			
		</div><!-- end row -->
	</div><!-- End container-->
</section>

<?php  include("_includes/footer.php") ?>