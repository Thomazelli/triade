<?php

	include("functions/planos.class.php");	

	$idplano = (int) $_GET['plano'];
	$planos = Planos::listar(" WHERE plan_id = " . $idplano );	

	if(!$planos){
		die("Plano inv�lido!");
	}	

	$carac = Planos::listar_caracteristicas($idplano);

?>

<?php  include("_includes/header.php") ?>

<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Falta pouco!</h1>
			</div>
		</div><!-- End row -->
	</div><!-- End container -->
	<div class="divider_top"></div>
</section><!-- End sub-header -->

<section id="main_content" >
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4>iMovel | Plano <?=$planos[0]->getTitulo()?></h4>
				<ul id="contact-info">
					<li>R$ <?=$planos[0]->getValor()?> / m&ecirc;s</li>
					<? foreach($carac as $item){ ?>
						<li><?=strip_tags($item->getCaracteristicas())?></li>
					<? } ?>
				</ul>
				<hr>
			</div>			

			<div class="col-md-8">
				<div class=" box_style_2">
					<div class="row">
						<div class="col-md-12">
							<h3>Dados pessoais</h3>
						</div>
					</div>

					<div id="message-apply"></div>

					<form method="post" action="concluir.php?plano=<?=$planos[0]->getPlanId()?>" onSubmit="return validaform()">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" id="frm_cpf" name="cpf" placeholder="CPF (Somente n&uacute;meros)">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" id="frm_nome" name="nome" placeholder="Nome">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="email" id="frm_email" name="email" class="form-control" placeholder="E-mail">
									<span class="input-icon"><i class="icon-email"></i></span>
								</div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="frm_telefone" name="telefone" class="form-control" placeholder="Telefone">
									<span class="input-icon"><i class="icon-phone"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="frm_nascimento" name="nascimento" class="form-control" placeholder="Data de nascimento">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-12">
								<h3>Dados de localiza&ccedil;&atilde;o</h3>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-9 col-sm-9">
								<div class="form-group">
									<input type="text" id="frm_endereco" name="endereco" class="form-control" placeholder="Endere&ccedil;o">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>

							<div class="col-md-3 col-sm-3">
								<div class="form-group">
									<input type="text" id="frm_numero" name="numero" class="form-control" placeholder="N&#176;">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="frm_complemento" name="complemento" class="form-control" placeholder="Complemento">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>

							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="frm_bairro" name="bairro" class="form-control" placeholder="Bairro">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-3 col-sm-3">
								<div class="form-group">
									<div class="styled-select">
										<select class="form-control" name="estado" id="frm_estado">
											<option value="" selected>Estado</option>
											<option value="AC">Acre</option>
											<option value="AL">Alagoas</option>
											<option value="AP">Amap&aacute;</option>
											<option value="AM">Amazonas</option>
											<option value="BA">Bahia</option>
											<option value="CE">Cear&aacute;</option>
											<option value="DF">Distrito Federal</option>
											<option value="ES">Espirito Santo</option>
											<option value="GO">Goi&aacute;s</option>
											<option value="MA">Maranh&atilde;o</option>
											<option value="MS">Mato Grosso do Sul</option>
											<option value="MT">Mato Grosso</option>
											<option value="MG">Minas Gerais</option>
											<option value="PA">Par&aacute;</option>
											<option value="PB">Para&iacute;ba</option>
											<option value="PR">Paran&aacute;</option>
											<option value="PE">Pernambuco</option>
											<option value="PI">Piau&iacute;</option>
											<option value="RJ">Rio de Janeiro</option>
											<option value="RN">Rio Grande do Norte</option>
											<option value="RS">Rio Grande do Sul</option>
											<option value="RO">Rond&ocirc;nia</option>
											<option value="RR">Roraima</option>
											<option value="SC">Santa Catarina</option>
											<option value="SP">S&atilde;o Paulo</option>
											<option value="SE">Sergipe</option>
											<option value="TO">Tocantins</option>
										</select>
									</div>
								</div>
							</div>

							<div class="col-md-9 col-sm-9">
								<div class="form-group">
									<input type="text" id="frm_cidade" name="cidade" class="form-control" placeholder="Cidade">
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>						

						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<div class="form-group pull-right">
									<input type="submit" value="Finalizar" class=" button_subscribe_green" id="submit-apply"/>
								</div>
							</div>
						</div>
					</form>					

					<script>
						function validaform(){
							if( !$("#frm_cpf").val() ){ alert("Preencha o CPF"); $("#frm_cpf").focus(); return false; }
							if( !$("#frm_nome").val() ){ alert("Preencha o Nome"); $("#frm_nome").focus(); return false; }
							if( !$("#frm_email").val() ){ alert("Preencha o E-mail"); $("#frm_email").focus(); return false; }
							if( !$("#frm_telefone").val() ){ alert("Preencha o Telefone"); $("#frm_telefone").focus(); return false; }
							if( !$("#frm_nascimento").val() ){ alert("Preencha o Nascimento"); $("#frm_nascimento").focus(); return false; }
							if( !$("#frm_endereco").val() ){ alert("Preencha o Endere�o"); $("#frm_endereco").focus(); return false; }
							if( !$("#frm_numero").val() ){ alert("Preencha o Numero"); $("#frm_numero").focus(); return false; }
							if( !$("#frm_bairro").val() ){ alert("Preencha o Bairro"); $("#frm_bairro").focus(); return false; }
							if( !$("#frm_estado").val() ){ alert("Selecione um estado"); $("#fm_estado").focus(); return false; }
							if( !$("#frm_cidade").val() ){ alert("Preencha a cidade"); $("#frm_cidade").focus(); return false; }
						}
					</script>
				</div>
			</div>
		</div>
	</div>
</section>

<?php  include("_includes/footer.php") ?>