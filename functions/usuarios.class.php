<?php
/*****
 * @Classe: Usuarios
 * @Autor: Gustavo Thomazelli
 * @Vers�o: 1
 * @Data: 22/05/2016 - 15:26:00 
 *****/
 
 require_once("db.class.php");

 class Usuarios extends Db {
     private $usu_id; // int
     private $usu_cpf; // int
     private $usu_email; // varchar
     private $usu_nome; // varchar
     private $usu_nascimento; // varchar
     private $usu_telefone; // varchar
     private $usu_endereco; // varchar
     private $usu_numero; // varchar
     private $usu_complemento; // varchar
     private $usu_bairro; // varchar
     private $usu_estado; // varchar
     private $usu_cidade; // varchar

     /**
      * @return int 
      **/
     public function getUsu_id(){
         return $this->usu_id;
     }

     /**
     * @param int $usu_id 
     **/
     public function setUsu_id($usu_id){
         $this->usu_id = $usu_id;
     }

     /**
      * @return int 
      **/
     public function getUsu_cpf(){
         return $this->usu_cpf;
     }

     /**
     * @param int $usu_cpf 
     **/
     public function setUsu_cpf($usu_cpf){
         $this->usu_cpf = $usu_cpf;
     }

     /**
      * @return varchar 
      **/
     public function getUsu_email(){
         return utf8_encode($this->usu_email);
     }

     /**
     * @param varchar $usu_email 
     **/
     public function setUsu_email($usu_email){
         $this->usu_email = utf8_decode($usu_email);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_nome(){
         return utf8_encode($this->usu_nome);
     }

     /**
     * @param varchar $usu_nome 
     **/
     public function setUsu_nome($usu_nome){
         $this->usu_nome = utf8_decode($usu_nome);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_nascimento(){
         return utf8_encode($this->usu_nascimento);
     }

     /**
     * @param varchar $usu_nascimento 
     **/
     public function setUsu_nascimento($usu_nascimento){
         $this->usu_nascimento = utf8_decode($usu_nascimento);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_telefone(){
         return utf8_encode($this->usu_telefone);
     }

     /**
     * @param varchar $usu_telefone 
     **/
     public function setUsu_telefone($usu_telefone){
         $this->usu_telefone = utf8_decode($usu_telefone);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_endereco(){
         return utf8_encode($this->usu_endereco);
     }

     /**
     * @param varchar $usu_endereco 
     **/
     public function setUsu_endereco($usu_endereco){
         $this->usu_endereco = utf8_decode($usu_endereco);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_numero(){
         return utf8_encode($this->usu_numero);
     }

     /**
     * @param varchar $usu_numero 
     **/
     public function setUsu_numero($usu_numero){
         $this->usu_numero = utf8_decode($usu_numero);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_complemento(){
         return utf8_encode($this->usu_complemento);
     }

     /**
     * @param varchar $usu_complemento 
     **/
     public function setUsu_complemento($usu_complemento){
         $this->usu_complemento = utf8_decode($usu_complemento);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_bairro(){
         return utf8_encode($this->usu_bairro);
     }

     /**
     * @param varchar $usu_bairro 
     **/
     public function setUsu_bairro($usu_bairro){
         $this->usu_bairro = utf8_decode($usu_bairro);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_estado(){
         return utf8_encode($this->usu_estado);
     }

     /**
     * @param varchar $usu_estado 
     **/
     public function setUsu_estado($usu_estado){
         $this->usu_estado = utf8_decode($usu_estado);
     }

     /**
      * @return varchar 
      **/
     public function getUsu_cidade(){
         return utf8_encode($this->usu_cidade);
     }

     /**
     * @param varchar $usu_cidade 
     **/
     public function setUsu_cidade($usu_cidade){
         $this->usu_cidade = utf8_decode($usu_cidade);
     }

     /*****
     * Listar - listar() ou listar(string filtro) ou (int pagina, int itensPorPagina)
     * @return Usuarios[]
     ******/

     public static function listar(){
         $Usuarios; //Array de objeto a ser retornado
         $args = func_get_args();

         try{
             $sql = "
                 SELECT
                     usu_id AS USU_ID,
                     usu_cpf AS USU_CPF,
                     usu_email AS USU_EMAIL,
                     usu_nome AS USU_NOME,
                     usu_nascimento AS USU_NASCIMENTO,
                     usu_telefone AS USU_TELEFONE,
                     usu_endereco AS USU_ENDERECO,
                     usu_numero AS USU_NUMERO,
                     usu_complemento AS USU_COMPLEMENTO,
                     usu_bairro AS USU_BAIRRO,
                     usu_estado AS USU_ESTADO,
                     usu_cidade AS USU_CIDADE
                 FROM
                     usuarios";
             if(count($args) == 1){
                 $sql .= " ".$args[0];
             }
             //instanciar um objeto banco de dados
             $db = new Db;
             //abrir a conexao
             $db->connect();
             //executar o select da instrucao sql
             $res = $db->select($sql);
             //adicionar ao vetor como objetos
             for($linha = 0; $linha < $res['total']; $linha++){
                 $Usuarios[$linha] = new Usuarios();
                 $Usuarios[$linha]->usu_id = $res[$linha]['USU_ID'];
                 $Usuarios[$linha]->usu_cpf = $res[$linha]['USU_CPF'];
                 $Usuarios[$linha]->usu_email = $res[$linha]['USU_EMAIL'];
                 $Usuarios[$linha]->usu_nome = $res[$linha]['USU_NOME'];
                 $Usuarios[$linha]->usu_nascimento = $res[$linha]['USU_NASCIMENTO'];
                 $Usuarios[$linha]->usu_telefone = $res[$linha]['USU_TELEFONE'];
                 $Usuarios[$linha]->usu_endereco = $res[$linha]['USU_ENDERECO'];
                 $Usuarios[$linha]->usu_numero = $res[$linha]['USU_NUMERO'];
                 $Usuarios[$linha]->usu_complemento = $res[$linha]['USU_COMPLEMENTO'];
                 $Usuarios[$linha]->usu_bairro = $res[$linha]['USU_BAIRRO'];
                 $Usuarios[$linha]->usu_estado = $res[$linha]['USU_ESTADO'];
                 $Usuarios[$linha]->usu_cidade = $res[$linha]['USU_CIDADE'];
             }
             //fechar conexao
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
         return $Usuarios;
     }

     /**
     * Detalhar - baseado nos atributos j� setados
     */
     public function detalhar(){
         try{
            $sql = "
                 SELECT
                     usu_id AS USU_ID,
                     usu_cpf AS USU_CPF,
                     usu_email AS USU_EMAIL,
                     usu_nome AS USU_NOME,
                     usu_nascimento AS USU_NASCIMENTO,
                     usu_telefone AS USU_TELEFONE,
                     usu_endereco AS USU_ENDERECO,
                     usu_numero AS USU_NUMERO,
                     usu_complemento AS USU_COMPLEMENTO,
                     usu_bairro AS USU_BAIRRO,
                     usu_estado AS USU_ESTADO,
                     usu_cidade AS USU_CIDADE
                 FROM
                         usuarios
                 WHERE ";

            if($this->id){
                $sql .= " id = ".$this->id;
            }else{
                throw new Exception("Falha na consulta");
            }
            //instancia um objeto banco de dados
            $db = new Db;
            //abre a conexao
            $db->connect();
            //executa o select da instrucao sql
            $res = $db->select($sql);
            //adiciona ao vetor como objetos
            $this->usu_id = $res['0']['USU_ID'];
            $this->usu_cpf = $res['0']['USU_CPF'];
            $this->usu_email = $res['0']['USU_EMAIL'];
            $this->usu_nome = $res['0']['USU_NOME'];
            $this->usu_nascimento = $res['0']['USU_NASCIMENTO'];
            $this->usu_telefone = $res['0']['USU_TELEFONE'];
            $this->usu_endereco = $res['0']['USU_ENDERECO'];
            $this->usu_numero = $res['0']['USU_NUMERO'];
            $this->usu_complemento = $res['0']['USU_COMPLEMENTO'];
            $this->usu_bairro = $res['0']['USU_BAIRRO'];
            $this->usu_estado = $res['0']['USU_ESTADO'];
            $this->usu_cidade = $res['0']['USU_CIDADE'];
            //fechar conexao
            $db->close();
            if($res['total']==0)
                throw new Exception("Nenhum resultado");
         }catch(Exception $e){
            throw $e;
         }
     }

     /**
     * Salvar - Inserir / Editar
     **/
     public function salvar(){
         try{
            if(!$this->id){
                $this->inserir();
            }else{
                $this->editar();
            }
         }catch(Exception $e){
            throw $e;
         }
     }
         
     /**
     * Inserir
     **/
     private function inserir(){
         $sql = "INSERT INTO 
         usuarios 
         (
             usu_id,
             usu_cpf,
             usu_email,
             usu_nome,
             usu_nascimento,
             usu_telefone,
             usu_endereco,
             usu_numero,
             usu_complemento,
             usu_bairro,
             usu_estado,
             usu_cidade
         )
         VALUES
         (
            0,
            ".(($this->usu_cpf)?($this->usu_cpf):"DEFAULT").",
            ".(($this->usu_email)?"'".addslashes($this->usu_email)."'":"DEFAULT").",
            ".(($this->usu_nome)?"'".addslashes($this->usu_nome)."'":"DEFAULT").",
            ".(($this->usu_nascimento)?"'".addslashes($this->usu_nascimento)."'":"DEFAULT").",
            ".(($this->usu_telefone)?"'".addslashes($this->usu_telefone)."'":"DEFAULT").",
            ".(($this->usu_endereco)?"'".addslashes($this->usu_endereco)."'":"DEFAULT").",
            ".(($this->usu_numero)?"'".addslashes($this->usu_numero)."'":"DEFAULT").",
            ".(($this->usu_complemento)?"'".addslashes($this->usu_complemento)."'":"DEFAULT").",
            ".(($this->usu_bairro)?"'".addslashes($this->usu_bairro)."'":"DEFAULT").",
            ".(($this->usu_estado)?"'".addslashes($this->usu_estado)."'":"DEFAULT").",
            ".(($this->usu_cidade)?"'".addslashes($this->usu_cidade)."'":"DEFAULT")." 
         )";
         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $GLOBALS['TEMP_VAR_A'] = $db->insertId();
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }
         
     /**
     * Editar
     **/
     private function editar(){
         $sql = "UPDATE
             usuarios 
         SET
             usu_cpf = ".(($this->usu_cpf)?($this->usu_cpf):"DEFAULT").",
             usu_email = ".(($this->usu_email)?"'".addslashes($this->usu_email)."'":"DEFAULT").",
             usu_nome = ".(($this->usu_nome)?"'".addslashes($this->usu_nome)."'":"DEFAULT").",
             usu_nascimento = ".(($this->usu_nascimento)?"'".addslashes($this->usu_nascimento)."'":"DEFAULT").",
             usu_telefone = ".(($this->usu_telefone)?"'".addslashes($this->usu_telefone)."'":"DEFAULT").",
             usu_endereco = ".(($this->usu_endereco)?"'".addslashes($this->usu_endereco)."'":"DEFAULT").",
             usu_numero = ".(($this->usu_numero)?"'".addslashes($this->usu_numero)."'":"DEFAULT").",
             usu_complemento = ".(($this->usu_complemento)?"'".addslashes($this->usu_complemento)."'":"DEFAULT").",
             usu_bairro = ".(($this->usu_bairro)?"'".addslashes($this->usu_bairro)."'":"DEFAULT").",
             usu_estado = ".(($this->usu_estado)?"'".addslashes($this->usu_estado)."'":"DEFAULT").",
             usu_cidade = ".(($this->usu_cidade)?"'".addslashes($this->usu_cidade)."'":"DEFAULT")."
         WHERE
             id = ".$this->id;
         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }
         
     /***** 
     * Excluir
     ******/
     public function excluir(){
         $sql = "DELETE FROM
             usuarios 
         WHERE
             id = ".$this->id;
         try{
             $db = new Db;
             $db->connect();
             $db->affectedRows($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }
 }
 ?>