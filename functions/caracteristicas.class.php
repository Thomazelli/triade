<?php

require_once("db.class.php");

class Caracteristicas extends Db {
    private $caracId;
    private $caracDescricao;

    public function getCaracId() {
        return $this->caracId;
    }

    public function setCaracId($caracId) {
        $this->caracId = $caracId;
    }

    public function getCaracDescricao() {
        return $this->caracDescricao;
    }

    public function setCaracDescricao($caracDescricao) {
        $this->caracDescricao = $caracDescricao;
    }

	/* DAO */
	public static function listar(){
		$Result;
		$args = func_get_args();		

		try{
			$sql = "SELECT * FROM caracteristicas";			

			if(count($args) == 1){
				$sql .= " ".$args[0];
			}

			$db = new Db();
			$db->connect();
			$res = $db->select( $sql );

			for($linha=0; $linha < $res['total']; $linha++){
				$Result[$linha] = new Caracteristicas();
				$Result[$linha]->id = $res[$linha]['carac_id'];
				$Result[$linha]->descricao = $res[$linha]['carac_descricao'];
			}			

			$db->close();			

		}catch(Exception $e){
			throw $e;
		}		

		return $Result;
	}

	public function salvar(){
         try{
            if(!$this->caracId){
                $this->inserir();
            }else{
                $this->editar();
            }
         }catch(Exception $e){
            throw $e;
         }
     }         

     /**
     * Inserir
     **/
     private function inserir(){
         $sql = "INSERT INTO 
         caracteristicas 
         (
             carac_id,
             carac_descricao
         )
         VALUES
         (
            0,
            ".(($this->caracDescricao)?"'".addslashes($this->caracDescricao)."'":"DEFAULT")."
         )";		 

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $GLOBALS['TEMP_VAR_A'] = $db->insertId();
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }         

     /**
     * Editar
     **/
     private function editar(){
         $sql = "UPDATE
             caracteristicas 
         SET
             carac_descricao = ".(($this->caracDescricao)?"'".addslashes($this->caracDescricao)."'":"DEFAULT")."
         WHERE
             carac_id = ".$this->caracId;

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }         

     /***** 
     * Excluir
     ******/
     public function excluir(){
         $sql = "DELETE FROM
             caracteristicas 
         WHERE
             carac_id = ".$this->caracId;

         try{
             $db = new Db;
             $db->connect();
             $db->affectedRows($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }

     public function del_carac_id(){
         $sql = "DELETE FROM
             planos_caracteristicas 
         WHERE
             car_id = ".$this->caracId;

         try{
             $db = new Db;
             $db->connect();
             $db->exec($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }

}

?>