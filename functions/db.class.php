<?php

/*****

 * Manipulacao do banco de dados MySQL

 * @Author - Gustavo Thomazelli

 * @Version 3.0 20/05/2016

 *****/

class Db{



	private $con;

	public function connect()
	{
		try{
			$this->con = new PDO('mysql:host=gustavothomazelli.com.br;dbname=gusta690_triade', 'gusta690_triade', 'triade1234');
			if( mysql_error() ){
				throw new Exception("Falha ao conectar.");
			}
		}catch(Exception $e){
			die("Falha ao conectar.");
		}	
	}	

	public function close()
	{
		try{
			@mysql_close($this->con);
			if(mysql_error()){
				throw new Exception("Falha ao fechar conex�o.");
			}
		}catch(Exception $e){
			throw $e;
		}		
	}

	public function exec($sql){
		try{
			$consulta = @$this->con->exec($sql);
		}catch(Exception $e){
			throw $e;
		}	
	}

	public function query($sql)
	{
		try{
			$consulta = @$this->con->exec($sql);
			if(!$consulta){
				throw new Exception("N�o h� valores afetados pela Query.");
			};
		}catch(Exception $e){
			throw $e;
		}	
	}

	public function select($sql){
		try{
			$consulta = @$this->con->query($sql);
			if($consulta){
				$resultado = $consulta->fetchAll();
				if($resultado){
					$resultado['total'] = count($resultado);
				} else {
					throw new Exception("N�o houve retorno de valores na Query.");
				};
			} else {
				throw new Exception("N�o h� valores na Query.");
			};
		} catch(Exception $e) {
			$resultado['error'] = $e->getMessage();
		};

		return $resultado;
	}	

	public function insertId(){
		try{
			$id = @$this->con->lastInsertId();
			if(!$id){
				throw new Exception('N�o houve retorno de valores na Query.');
			};
		}catch(Exception $e){
			throw $e;
		}	

		return $id;
	}

	public function affectedRows($sql){
		try{
			$consulta = @$this->con->exec($sql);
			if(!$consulta){
				throw new Exception("N�o h� valores afetados pela Query.");
			};
		}catch(Exception $e){
			throw $e;
		}
	}

}

?>