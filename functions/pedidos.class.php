<?php

/*****

 * @Classe: Pedidos

 * @Autor: Gustavo Thomazelli

 * @Vers�o: 1

 * @Data: 22/05/2016 - 15:41:15 

 *****/



 require_once("db.class.php");

 

 class Pedidos {

     public $ped_id; // int
     public $usu_id; // int
     public $plan_id; // int
     public $ped_valor; // outro
     public $ped_data_compra; // varchar
     public $status_id; // int
     public $status_desc; // varchar
     public $usu_nome; // varchar
     public $usu_email; // varchar
	 public $plan_titulo; // varchar

     /**
      * @return int 
      **/
     public function getPed_id(){
         return $this->ped_id;
     }

     /**
     * @param int $ped_id 
     **/
     public function setPed_id($ped_id){
         $this->ped_id = $ped_id;
     }

     /**
      * @return int 
      **/
     public function getUsu_id(){
         return $this->usu_id;
     }

     /**
     * @param int $usu_id 
     **/
     public function setUsu_id($usu_id){
         $this->usu_id = $usu_id;
     }

     /**
      * @return int 
      **/
     public function getPlan_id(){
         return $this->plan_id;
     }

     /**
     * @param int $plan_id 
     **/
     public function setPlan_id($plan_id){
         $this->plan_id = $plan_id;
     }

     /**
      * @return outro 
      **/
     public function getPed_valor(){
         return $this->ped_valor;
     }

     /**
     * @param outro $ped_valor 
     **/
     public function setPed_valor($ped_valor){
         $this->ped_valor = $ped_valor;
     }

     /**
      * @return varchar 
      **/
     public function getPed_data_compra(){
         return utf8_encode($this->ped_data_compra);
     }

     /**
     * @param varchar $ped_data_compra 
     **/
     public function setPed_data_compra($ped_data_compra){
         $this->ped_data_compra = utf8_decode($ped_data_compra);
     }

     /**
      * @return int 
      **/
     public function getStatus_id(){
         return $this->status_id;
     }

     /**
     * @param int $status_id 
     **/
     public function setStatus_id($status_id){
         $this->status_id = $status_id;
     }	 

	 /**
      * @return varchar
      **/
     public function getStatus_desc(){
         return utf8_encode($this->status_desc);
     }

     /**
     * @param varchar $ped_data_compra 
     **/
     public function setStatus_desc($status_desc){
         $this->status_desc = utf8_decode($status_desc);
     }

     /*****
     * Listar - listar() ou listar(string filtro) ou (int pagina, int itensPorPagina)
     * @return Pedidos[]
     ******/

     public static function listar(){
         $Pedidos; //Array de objeto a ser retornado
         $args = func_get_args();

         try{
             $sql = "
                 SELECT
                     pedidos.ped_id AS PED_ID,
                     pedidos.usu_id AS USU_ID,
                     pedidos.plan_id AS PLAN_ID,
                     pedidos.ped_valor AS PED_VALOR,
                     pedidos.ped_data_compra AS PED_DATA_COMPRA,
                     pedidos.status_id AS STATUS_ID,
                     status.status_descricao AS STATUS_DESCRICAO,
                     usuarios.usu_nome AS USU_NOME,
                     usuarios.usu_email AS USU_EMAIL,
					 planos.plan_titulo AS PLAN_TITULO
                 FROM
                     pedidos
				LEFT JOIN status ON status.status_id = pedidos.status_id
                LEFT JOIN usuarios ON usuarios.usu_id = pedidos.usu_id
                LEFT JOIN planos ON planos.plan_id = pedidos.plan_id
                ";

             if(count($args) == 1){
                 $sql .= " ".$args[0];
             }			 

			 //echo $sql;		 

             //instanciar um objeto banco de dados
             $db = new Db;

             //abrir a conexao
             $db->connect();

             //executar o select da instrucao sql
             $res = $db->select($sql);

             //adicionar ao vetor como objetos
             for($linha = 0; $linha < $res['total']; $linha++){
                 $Pedidos[$linha] = new Pedidos();
                 $Pedidos[$linha]->ped_id = $res[$linha]['PED_ID'];
                 $Pedidos[$linha]->usu_id = $res[$linha]['USU_ID'];
                 $Pedidos[$linha]->plan_id = $res[$linha]['PLAN_ID'];
                 $Pedidos[$linha]->ped_valor = $res[$linha]['PED_VALOR'];
                 $Pedidos[$linha]->ped_data_compra = $res[$linha]['PED_DATA_COMPRA'];
                 $Pedidos[$linha]->status_id = $res[$linha]['STATUS_ID'];
                 $Pedidos[$linha]->status_desc = $res[$linha]['STATUS_DESCRICAO'];
                 $Pedidos[$linha]->usu_nome = $res[$linha]['USU_NOME'];
                 $Pedidos[$linha]->usu_email = $res[$linha]['USU_EMAIL'];
				 $Pedidos[$linha]->plan_titulo = $res[$linha]['PLAN_TITULO'];
             }
             //fechar conexao

             $db->close();
         }catch(Exception $e){
             throw $e;
         }

         return $Pedidos;
     }

     /**
     * Detalhar - baseado nos atributos j� setados
     */
     public function detalhar(){
         try{
            $sql = "
                 SELECT
                     pedidos.ped_id AS PED_ID,
                     pedidos.usu_id AS USU_ID,
                     pedidos.plan_id AS PLAN_ID,
                     pedidos.ped_valor AS PED_VALOR,
                     pedidos.ped_data_compra AS PED_DATA_COMPRA,
                     pedidos.status_id AS STATUS_ID,
                     usuarios.usu_nome AS USU_NOME,
                     usuarios.usu_email AS USU_EMAIL,
                     planos.plan_titulo AS PLAN_TITULO
                 FROM
                         pedidos
                 LEFT JOIN status ON status.status_id = pedidos.status_id
                 LEFT JOIN usuarios ON usuarios.usu_id = pedidos.usu_id
                 LEFT JOIN planos ON planos.plan_id = pedidos.plan_id
                 WHERE ";

            if($this->ped_id){
                $sql .= " ped_id = ".$this->ped_id;
            }else{
                throw new Exception("Falha na consulta");
            }

            //instancia um objeto banco de dados
            $db = new Db;

            //abre a conexao
            $db->connect();

            //executa o select da instrucao sql
            $res = $db->select($sql);

            //adiciona ao vetor como objetos
            $this->ped_id = $res['0']['PED_ID'];
            $this->usu_id = $res['0']['USU_ID'];
            $this->plan_id = $res['0']['PLAN_ID'];
            $this->ped_valor = $res['0']['PED_VALOR'];
            $this->ped_data_compra = $res['0']['PED_DATA_COMPRA'];
            $this->status_id = $res['0']['STATUS_ID'];
            $this->usu_nome = $res['0']['USU_NOME'];
            $this->usu_email = $res['0']['USU_EMAIL'];
            $this->plan_titulo = $res['0']['PLAN_TITULO'];
            //fechar conexao

            $db->close();

            if($res['total']==0)
                throw new Exception("Nenhum resultado");
         }catch(Exception $e){
            throw $e;
         }
     }

     /**
     * Salvar - Inserir / Editar
     **/
     public function salvar(){
         try{
            if(!$this->ped_id){
                $this->inserir();
            }else{
                $this->editar();
            }
         }catch(Exception $e){
            throw $e;
         }
     }         

     /**
     * Inserir
     **/
     private function inserir(){
         $sql = "INSERT INTO 
         pedidos 
         (
             ped_id,
             usu_id,
             plan_id,
             ped_valor,
             ped_data_compra,
             status_id
         )
         VALUES
         (
            0,
            ".(($this->usu_id)?($this->usu_id):"DEFAULT").",
            ".(($this->plan_id)?($this->plan_id):"DEFAULT").",
            ".(($this->ped_valor)?($this->ped_valor):"DEFAULT").",
            ".(($this->ped_data_compra)?"'".addslashes($this->ped_data_compra)."'":"DEFAULT").",
            ".(($this->status_id)?($this->status_id):"DEFAULT")." 
         )";		 

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $GLOBALS['TEMP_VAR_A'] = $db->insertId();
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }         

     /**
     * Editar
     **/
     private function editar(){
         $sql = "UPDATE
             pedidos 
         SET
             usu_id = ".(($this->usu_id)?($this->usu_id):"DEFAULT").",
             plan_id = ".(($this->plan_id)?($this->plan_id):"DEFAULT").",
             ped_valor = ".(($this->ped_valor)?($this->ped_valor):"DEFAULT").",
             ped_data_compra = ".(($this->ped_data_compra)?"'".addslashes($this->ped_data_compra)."'":"DEFAULT").",
             status_id = ".(($this->status_id)?($this->status_id):"DEFAULT")."
         WHERE
             ped_id = ".$this->ped_id;

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }         

     /***** 
     * Excluir
     ******/
     public function excluir(){
         $sql = "DELETE FROM
             pedidos 
         WHERE
             ped_id = ".$this->ped_id;

         try{
             $db = new Db;
             $db->connect();
             $db->affectedRows($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }
     
 }

 ?>