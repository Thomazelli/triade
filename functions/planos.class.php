<?php

require_once("db.class.php");

class Planos extends Db {
    public $id;
    public $planId;
    public $prodId;
    public $titulo;
    public $valor;	

	public $caracteristicas;

    public function getPlanId() {
        return $this->planId;
    }

    public function setPlanId($planId) {
        $this->planId = $planId;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setTitulo($titulo) {
        $this->titulo = strtolower($titulo);
    }

    public function getValor() {
        return $this->valor;
    }

    public function setValor($valor) {
        $this->valor = strtolower($valor);
    }	

	public function getCaracteristicas() {
        return $this->caracteristicas;
    }

    public function setCaracteristicas($caracteristicas) {
        $this->caracteristicas = strtolower($caracteristicas);
    }	

	/* DAO */
	public static function listar(){
		$Result;
		$args = func_get_args();		

		try{
			$sql = "SELECT * FROM planos";			

			if(count($args) == 1){
				$sql .= " ".$args[0];
			}	

			$db = new Db();
			$db->connect();
			$res = $db->select( $sql );			

			for($linha=0; $linha < $res['total']; $linha++){
				$Result[$linha] = new Planos();
				$Result[$linha]->planId = $res[$linha]['plan_id'];
				$Result[$linha]->titulo = $res[$linha]['plan_titulo'];
				$Result[$linha]->valor = $res[$linha]['plan_valor'];
			}			

			$db->close();			

		}catch(Exception $e){
			throw $e;
		}		

		return $Result;
	}	

	public static function listar_caracteristicas( $planid ){
		$Result;
		$args = func_get_args();		

		try{
			$sql = "SELECT 
						carac_id as ID,
						carac_descricao as DESCRICAO
					FROM caracteristicas 
					WHERE carac_id in 
						(SELECT car_id 
							FROM planos_caracteristicas 
							WHERE plan_id = " . $planid . "
						)";			

			//echo $sql;			

			$db = new Db();
			$db->connect();
			$res = $db->select( $sql );			

			for($linha=0; $linha < $res['total']; $linha++){
				$Result[$linha] = new Planos();
				$Result[$linha]->id = $res[$linha]['ID'];
				$Result[$linha]->caracteristicas = $res[$linha]['DESCRICAO'];
			}			

			$db->close();
		}catch(Exception $e){
			throw $e;
		}		

		return $Result;
	}

	public function salvar(){
         try{
            if(!$this->planId){
                $this->inserir();
            }else{
                $this->editar();
            }
         }catch(Exception $e){
            throw $e;
         }
     }

     /**
     * Inserir
     **/
     private function inserir(){
         $sql = "INSERT INTO 
         planos 
         (
             plan_id,
             plan_titulo,
             plan_valor
         )
         VALUES
         (
            0,
            ".(($this->titulo)?"'".addslashes(strtoupper($this->titulo))."'":"DEFAULT").",
            ".(($this->valor)?($this->valor):"DEFAULT")."
         )";		 

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $GLOBALS['TEMP_VAR_A'] = $db->insertId();
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }         

     /**
     * Editar
     **/
     private function editar(){
         $sql = "UPDATE
             planos 
         SET
             plan_titulo = ".(($this->titulo)?"'".addslashes(strtoupper($this->titulo))."'":"DEFAULT").",
             plan_valor = ".(($this->valor)?($this->valor):"DEFAULT")."
         WHERE
             plan_id = ".$this->planId;

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }

     public function excluir(){
         $sql = "DELETE FROM
             planos 
         WHERE
             plan_id = ".$this->planId;

         try{
             $db = new Db;
             $db->connect();
             $db->affectedRows($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }

     public function del_carac(){
     	$sql = "DELETE FROM
             planos_caracteristicas 
         WHERE
             plan_id = ".$this->planId;

         try{
             $db = new Db;
             $db->connect();
             $db->exec($sql);
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     }

     public function ins_carac($plano, $carac){
         $sql = "INSERT INTO 
         planos_caracteristicas 
         (
             plan_id,
             car_id
         )
         VALUES
         (
            ".$plano.",
            ".$carac."
         )";

         try{
             $db = new Db;
             $db->connect();
             $db->query($sql);
             //$GLOBALS['TEMP_VAR_A'] = $db->insertId();
             $db->close();
         }catch(Exception $e){
             throw $e;
         }
     } 

}

?>