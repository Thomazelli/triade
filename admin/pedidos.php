<?php
	include("../functions/pedidos.class.php");

	$status = (int) $_GET['situacao'];

	if( $status ){
		$filtro = 'WHERE pedidos.status_id = ' . $status;
	}
	
	$pedidos = Pedidos::listar( $filtro . ' ORDER BY ped_id desc' );
?>

<? include('inc/header.php') ?>

<h1 class="m-b-40">Pedidos</h1>

<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Usuário</th>
					<th>E-mail</th>
					<th>Plano</th>
					<th>Valor</th>
					<th>Status</th>
					<th class="text-center">Ação</th>
				</tr>
			</thead>
			<tbody>
				<? if($pedidos){ ?>

					<? foreach( $pedidos as $pedido ){ ?>
						<tr>
							<td> <?=$pedido->ped_id?> </td>
							<td> <?=$pedido->usu_nome?> </td>
							<td> <?=$pedido->usu_email?> </td>
							<td> <?=$pedido->plan_titulo?> </td>
							<td> <?=$pedido->ped_valor?> </td>
							<td> <?=$pedido->status_desc?> </td>
							<td align="center"> 
								<? if( $pedido->status_id == 2){ ?>
									<a class="btn btn-xs btn-success" href="authorize.php?ped=<?=$pedido->ped_id?>" alt="Liberar pedido" title="Liberar pedido"><i class="glyphicon glyphicon-ok"></i></a> 
								<? } ?>
							</td>
						</tr>
					<? } ?>

				<? } ?>
			</tbody>
		</table>
	</div>
</div>

<? include('inc/footer.php') ?>