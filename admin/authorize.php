<?php

	include("../functions/planos.class.php");
	include("../functions/usuarios.class.php");
	include("../functions/pedidos.class.php");	

	$id = (int) $_GET['ped'];
	
	$pedido = new Pedidos();

	$pedido->setPed_id( $id );
	$pedido->detalhar();

	$pedido->setStatus_id(4);

	try{
		$pedido->salvar();
	}
	catch( Exception $e ){
		$error =  utf8_encode($e->getMessage());
	}

	if(!$error){
		$idpedido = $pedido->ped_id;			

		/* ENVIA EMAIL */
		$headers = "MIME-Version: 1.1\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: contato@triadesolutions.com.br\r\n"; // remetente
		$headers .= "Return-Path: contato@triadesolutions.com.br\r\n"; // return-path			

		$texto = '
			Olá, ' .$pedido->usu_nome. ', <br />
			O seu plano <strong>'.$pedido->plan_titulo.'</strong> do iMovel está liberado para utilização!<br /><br />
			Os seus dados de acesso são:<br /><br />
			<strong>Usuário:</strong> '. $pedido->usu_email .'<br />
			<strong>Senha:</strong> pass'. rand(288, 987) .'<br /><br />
			<strong>Equipe Triade Solutions</strong>
		';			

		mail($pedido->usu_email, "iMovel | Pagamento aprovado", $texto, $headers);

		header("location:index.php");
	}else{
		die("Falha ao liberar pedido");
	}

	die();