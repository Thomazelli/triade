<?php
	include("../functions/pedidos.class.php");	
	
	$pedidos = Pedidos::listar(' WHERE pedidos.status_id in (2) ORDER BY ped_id desc');
?>

<? include('inc/header.php') ?>

<h1 class="m-b-40">Pedidos pendentes de liberação</h1>

<div class="row">
	<div class="col-md-12">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Usuário</th>
					<th>E-mail</th>
					<th>Plano</th>
					<th>Valor</th>
					<th>Status</th>
					<th>Ação</th>
				</tr>
			</thead>
			<tbody>
				<? if($pedidos){ ?>

					<? foreach( $pedidos as $pedido ){ ?>
						<tr>
							<td> <?=$pedido->ped_id?> </td>
							<td> <?=$pedido->usu_nome?> </td>
							<td> <?=$pedido->usu_email?> </td>
							<td> <?=$pedido->plan_titulo?> </td>
							<td> <?=$pedido->ped_valor?> </td>
							<td> <?=$pedido->status_desc?> </td>
							<td> <a href="authorize.php?ped=<?=$pedido->ped_id?>">Liberar acesso</a> </td>
						</tr>
					<? } ?>

				<? } ?>
			</tbody>
		</table>
	</div>
</div>

<? include('inc/footer.php') ?>