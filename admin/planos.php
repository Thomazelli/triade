<?php
	include("../functions/caracteristicas.class.php");
	include("../functions/planos.class.php");

	if($_POST){
		$id = (int) $_POST['id'];
		$plano = $_POST['plano'];
		$valor = $_POST['valor'];
		$carac = $_POST['caracteristicas'];

		$data = new Planos();
		if($id){ 
			$data->setPlanId($id); 
		}
		$data->setTitulo($plano);
		$data->setValor($valor);

		try{
			$data->salvar();
		}
		catch( Exception $e ){
			$error =  utf8_encode($e->getMessage());
		}

		try{
			$id = ($id) ? $id : $GLOBALS['TEMP_VAR_A'];

			$del = new Planos();
			$del->setPlanId($id);
			$del->del_carac();

			foreach( $carac as $item ){
				$car = new Planos();
				$car->ins_carac($id, $item);
			}
		}catch( Exception $e ){
			$error = utf8_encode($e->getMessage());
		}		

		if(!$error){
			header("location:planos.php");
		}else{
			header("location:planos.php");
		}
	}
	
	$caracteristicas = Caracteristicas::listar();
	$planos = Planos::listar();

	$id = (int) $_GET['id'];

	if($id){

		if($_GET['acao'] == 'del'){
			$del = new Planos();
			$del->setPlanId($id);
			$del->del_carac();
			$del->excluir();

			header("location:planos.php");
		}

		$edit = Planos::listar('WHERE plan_id = ' . $id);
		$edit_carac = Planos::listar_caracteristicas( $id );

		if( $edit_carac ){
			foreach( $edit_carac as $item ){
				$arr_carac[] = $item->id;
			}
		}
	}


?>

<? include('inc/header.php') ?>

<h1 class="m-b-40">Planos</h1>

<div class="row">
	<div class="col-md-5">
		<h3 class="m-b-20"><?= (!$id) ? 'Novo plano' : 'Editar plano' ?></h3>

		<form name="planos" action="planos.php" method="post">
			<input type="hidden" name="id" value="<?= ( $id ) ? $edit[0]->planId : '' ?>" />
			<div class="form-group">
				<label for="descricao">Plano:</label>
				<input type="text" name="plano" id="plano" class="form-control" value="<?= ( $id ) ? $edit[0]->titulo : '' ?>" required />
			</div>

			<div class="form-group">
				<label for="descricao">Valor:</label>
				<input type="number" name="valor" id="valor" class="form-control" value="<?= ( $id ) ? $edit[0]->valor : '' ?>" required />
			</div>

			<div class="form-group">
				<label for="descricao">Características:</label>
				<? foreach( $caracteristicas as $item ){ ?>
					<div class="checkbox">
						<label><input type="checkbox" name="caracteristicas[]" value="<?=$item->id?>" <?= ( $arr_carac && $id && in_array($item->id, $arr_carac ) ? 'checked' : '') ?>><?=$item->descricao?></label>
					</div>
				<? } ?>
			</div>

			<button type="submit" class="btn btn-primary pull-right">Gravar</button>
		</form>
	</div>

	<div class="col-md-7">
		<h3 class="m-b-10">Lista</h3>

		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Plano</th>
					<th>Valor</th>
					<th>Ação</th>
				</tr>
			</thead>
			<tbody>
				<? if($planos){ ?>

					<? foreach( $planos as $item ){ ?>
						<tr>
							<td> <?=$item->planId?> </td>
							<td> <?=$item->titulo?> </td>
							<td> <?=$item->valor?> </td>
							<td> 
								<a class="btn btn-xs btn-default" href="planos.php?id=<?=$item->planId?>"><i class="glyphicon glyphicon-edit"></i></a> 
								<a class="btn btn-xs btn-danger" href="planos.php?id=<?=$item->planId?>&acao=del"><i class="glyphicon glyphicon-remove"></i></a> 
							</td>
						</tr>
					<? } ?>

				<? } ?>
			</tbody>
		</table>
	</div>
</div>

<? include('inc/footer.php') ?>