<?php
	session_start();

	if($_POST){

		$user = $_POST['username'];
		$pass = $_POST['password'];

		if( $user == 'admin' && $pass == 'admin' ){
			$_SESSION['logged'] = true;
			header("location:index.php");
		}

	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <title>Painel administrativo</title>

	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">	    
	    <link rel="stylesheet" href="inc/style.css" />

	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>

	<body>
		<div class="container">
			<div class="row m-t-60">
				<div class="col-md-3"></div>

				<div class="col-md-6">
					<h1 class="text-center m-b-40">Triade ADMIN</h1>

					<form name="planos" action="login.php" method="post">
						<div class="form-group input-group input-group-lg">
							<span class="input-group-addon" id="basic-addon1">@</span>
							<input type="text" name="username" class="form-control" placeholder="Usuário" />
						</div>

						<div class="form-group input-group input-group-lg">
							<span class="input-group-addon" id="basic-addon1">@</span>
							<input type="password" name="password" class="form-control" placeholder="Senha" />
						</div>

						<button type="submit" class="btn btn-primary pull-right">Entrar</button>
					</form>
				</div>

				<div class="col-md-3"></div>
			</div>
		</div>

	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>