<?php
	include("../functions/caracteristicas.class.php");

	if($_POST){
		$id = (int) $_POST['id'];
		$descricao = $_POST['descricao'];

		$data = new Caracteristicas();
		if($id){ 
			$data->setCaracId($id); 
		}
		$data->setCaracDescricao($descricao);

		try{
			$data->salvar();
		}
		catch( Exception $e ){
			$error =  utf8_encode($e->getMessage());
		}

		if(!$error){
			header("location:caracteristicas.php");
		}else{
			die($error);
		}
	}
	
	$caracteristicas = Caracteristicas::listar();

	$id = (int) $_GET['id'];

	if($id){
		if($_GET['acao'] == 'del'){
			$del = new Caracteristicas();
			$del->setCaracId($id);
			$del->del_carac_id();
			$del->excluir();

			header("location:caracteristicas.php");
		}

		$edit = Caracteristicas::listar('WHERE carac_id = ' . $id);
	}


?>

<? include('inc/header.php') ?>

<h1 class="m-b-40">Características</h1>

<div class="row">
	<div class="col-md-5">
		<h3 class="m-b-20"><?= (!$id) ? 'Nova característica' : 'Editar característica' ?></h3>

		<form name="caracteristicas" action="caracteristicas.php" method="post">
			<input type="hidden" name="id" value="<?= ( $id ) ? $edit[0]->id : '' ?>" />
			<div class="form-group">
				<label for="descricao">Descrição:</label>
				<input type="text" name="descricao" id="descricao" class="form-control" value="<?= ( $id ) ? $edit[0]->descricao : '' ?>" required />
			</div>

			<button type="submit" class="btn btn-primary pull-right">Gravar</button>
		</form>
	</div>

	<div class="col-md-7">
		<h3 class="m-b-10">Lista</h3>

		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Característica</th>
					<th>Ação</th>
				</tr>
			</thead>
			<tbody>
				<? if($caracteristicas){ ?>

					<? foreach( $caracteristicas as $item ){ ?>
						<tr>
							<td> <?=$item->id?> </td>
							<td> <?=$item->descricao?> </td>
							<td> 
								<a class="btn btn-xs btn-default" href="caracteristicas.php?id=<?=$item->id?>"><i class="glyphicon glyphicon-edit"></i></a> 
								<a class="btn btn-xs btn-danger" href="caracteristicas.php?id=<?=$item->id?>&acao=del"><i class="glyphicon glyphicon-remove"></i></a> 
							</td>
						</tr>
					<? } ?>

				<? } ?>
			</tbody>
		</table>
	</div>
</div>

<? include('inc/footer.php') ?>