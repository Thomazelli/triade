<?php
	include("functions/planos.class.php");
	include("functions/pedidos.class.php");
	
	$idpedido = (int) $_GET['id'];
	$pedido = Pedidos::listar(" WHERE ped_id = " . $idpedido);
	
	if(!$pedido){
		die("Pedido inv�lido!");
	}
	
	$idplano = $pedido[0]->getPlan_id();
	$planos = Planos::listar(" WHERE plan_id = " . $idplano );
?>

<?php  include("_includes/header.php") ?>

<div id="middle-wizard">
	<div class="submit step" id="complete">
		<i class="icon-check"></i>
		<h3>Solicita&ccedil;&atilde;o efetuada!</h3>
		<h4>Situa&ccedil;&atilde;o: <strong><?=$pedido[0]->getStatus_desc()?></strong></h4>
		
		<? if($pedido[0]->getStatus_id() == 1){ ?>
			<form method="post" target="pagseguro" action="https://pagseguro.uol.com.br/v2/checkout/payment.html">  
          
				<!-- Campos obrigat�rios -->  
				<input name="receiverEmail" type="hidden" value="lgthomazelli@gmail.com">  
				<input name="currency" type="hidden" value="BRL">  
		  
				<!-- Itens do pagamento (ao menos um item � obrigat�rio) -->  
				<input name="itemId1" type="hidden" value="<?=$pedido[0]->getPlan_id()?>">  
				<input name="itemDescription1" type="hidden" value="iMovel | <?=$planos[0]->getTitulo()?>">  
				<input name="itemAmount1" type="hidden" value="<?=$pedido[0]->getPed_valor()?>">  
				<input name="itemQuantity1" type="hidden" value="1">  
				<input name="itemShippingCost1" type="hidden" value="0.00">  
		  
				<!-- C�digo de refer�ncia do pagamento no seu sistema (opcional) -->  
				<input name="reference" type="hidden" value="REF<?=$pedido[0]->getPed_id()?>">
		  
				<!-- submit do form (obrigat�rio) -->  
				<button type="submit" name="process" class="submit" style="margin-top:20px;">Efetuar pagamento</button>		
          
			</form>
		<? } ?>
	</div>
</div>

<?php  include("_includes/footer.php") ?>